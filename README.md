![ListFiltering Intro picture](person-holding-magnifying-glass-712786.jpg)

# List Filtering (Flutter)

## What was created

> A list of cards in flutter, that is scrollable and with filter by email or name and a few millisecond delay before processing filter

## Flutter version

Flutter 1.12.13+hotfix.5

## Dart version

Dart 2.7.0

## Reflections

- Filter by some property
- Running a run function that executes a callback based on a condition
- The aforementioned callback is used to delay the processing of the filter by milliseconds.


## By

> [Khairul A K](mailto:khairulkulma@gmail.com)

![ListFiltering End Banner](clear-glass-pitcher-beside-stainless-steel-kettle-2616170.jpg)
